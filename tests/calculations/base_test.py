# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Implements the calculation test cases.
"""


import pytest

from sample_calculator import calculations
from sample_calculator.calculations import base
from sample_calculator.calculations.calculations import sum_calculation


def test_creation_success():
    sum_calculation_ = base.create_calculation([calculations.SUM_CALCULATION])[0]
    assert sum_calculation_.name == calculations.SUM_CALCULATION


def test_handling_invalid_input_values():
    sum_calculation_ = base.create_calculation([calculations.SUM_CALCULATION])[0]
    with pytest.raises(TypeError):
        assert sum_calculation_.calculate(None)


def test_loop_detection():
    sum_calculation.SumCalculation.dependent_calculations.append(calculations.SUM_CALCULATION)
    with pytest.raises(calculations.CalculationError):
        calculations.create_calculation([calculations.SUM_CALCULATION])
    sum_calculation.SumCalculation.dependent_calculations.remove(calculations.SUM_CALCULATION)


def test_invalid_name():
    with pytest.raises(calculations.CalculationError):
        calculations.create_calculation([""])
